$(document).ready(function(){
    $.ajax({
        type: "GET",
        url: "nav.xml",
        dataType: "xml",
        success: function(xml){
            var ul_main=$("<ul id='list' />");
            //console.log($(xml).find("Menu"));
            $(xml).find("Menu").each(function(){
                
                if(window.location.href == $(this).attr("url")){ 
                    ul_main.append("<li class='active' id="+$(this).attr("id")+"><a href="+$(this).attr("url")+">"+$(this).attr("text")+"</a></li>");
                }else
                {
                    if($(this).attr("id") == 'login') 
                    {
                        if(document.getElementById('logged-in') !== null){
                            ul_main.append("<li id="+$(this).attr("id")+"><a href="+$(this).attr("url")+">Logout</a></li>");
                        }
                    }

                    ul_main.append("<li id="+$(this).attr("id")+"><a href="+$(this).attr("url")+">"+$(this).attr("text")+"</a></li>");
                }
            });

            $("#nav_bar").append(ul_main);
           
            
        }
    });
});

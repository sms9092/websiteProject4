<!DOCTYPE html>
<html>
<head>
    <title>imageVault</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="nav-bar.js"></script>
    <link rel="stylesheet" type="text/css" href="nav.css" media="screen" />

    <style>
       
.container {
  position: relative;
  width: 300px;
  
  
}

.image {
  width: 300px;
  height: 250px;
}

.overlay {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: grey;
  overflow: hidden;
  width: 100%;
  height: 0;
  transition: .5s ease;
}

.container:hover .overlay {
  height: 20%;
}

.text {
  color: white;
  font-size: 20px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}

.img-container{
display: grid;
   grid-template-columns: auto,auto,auto;
   grid-column-gap: 20px;
}

    </style>
</head>
<body>

<div id="nav_bar"></div>


<br>
<form action="process_imageUpload.php" method="post" enctype="multipart/form-data">
    <label>Author</label>
    <input type="text" name="Author" value="">
    <label>File Upload</label>
    <input type="File" name="file">
    <input type="submit" name="submit">
</form>

<div class="img-container">
<?php
    include("fetchImage.php");
?>
</div>



</body>
<script src="master.js"></script>
</html>


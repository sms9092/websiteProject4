<!DOCTYPE html>
<html>
<head>
    <title>imageVault</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="scripts/nav-bar.js"></script>
    <link rel="stylesheet" type="text/css" href="stylesheet/nav.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="stylesheet/style.css" media="screen" />

    <style>   
    form{
      padding: 10px 0 10px 0 ;
      background: dimgrey;
      position: sticky;
      border: 1px dashed black;
      text-align: center;
    }
.container {
  position: relative;
  width: 300px;
}

.image {
  width: 300px;
  height: 250px;
  border: 1px solid black;
  box-shadow: 1px;
}

.overlay {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: grey;
  overflow: hidden;
  width: 100%;
  height: 0;
  transition: .5s ease;
}

.container:hover .overlay {
  height: 20%;
}

.text {
  color: white;
  font-size: 20px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
}

.img-container{
  padding-top: 20px;
  display: grid;
  grid-template-columns: auto auto auto auto;
   grid-gap: 10px;
}
body{
  background: linear-gradient(120deg, #2980b9,#8e44ad);
}
</style>
</head>

<body>  
<div id="nav_bar"></div><br>
<?php
    session_start();
    if(isset($_SESSION['logged']))
    {
      echo "<div class ='image-form'><form action='process_imageUpload.php' method='post' enctype='multipart/form-data'>
      <label>Author</label>
      <input type='text' id='Author' name='Author' value=''>
      <label>File Upload</label>
      <input type='File' name='file'>
      <input type='submit' name='submit' value='Upload'>
       </form></div>";
      echo "<div id ='logged-in'></div>";
    }else
    echo "You need to log-in to upload photos ";
    ?>


<div class="img-container">
<?php
    include("fetchImage.php");
?>
</div>

<div id="myModal" class="modal">

  <!-- The Close Button -->
  <span class="close">&times;</span>

  <!-- Modal Content (The Image) -->
  <img class="modal-content" id="img01">

  <!-- Modal Caption (Image Text) -->
  <div id="caption"></div>
</div>

</body>
<script src="master.js"></script>
</html>


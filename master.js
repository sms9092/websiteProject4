function enlarge(url)
{
    //console.log(url.src);
    var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");

  modal.style.display = "block";
  modalImg.src = url.src;
  captionText.innerHTML = url.alt;


// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
} 
}

function jsonAjax() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      processJson(this);
    }
  };
  xmlhttp.open("GET", "data/projects.json", true);
  xmlhttp.send();
}
function processJson(json) {
  var obj = JSON.parse(json.responseText);
  display(obj);
  //taskCreation(obj);
}

function display(obj) {
    //console.log(obj.projects[1].image[0]);
    var text="";
    //traverse through the object by each item
  for(var i in obj.projects){
    text += "<div class='projects'><div class = 'divided'>" +
            "<div class='left-side'><div class='name'><h1>"+obj.projects[i].name +"</h1></div>" +
            "<div class ='decription'><p>"+ obj.projects[i].description +"</p></div>" +
            "<div class ='app-images'>";
            for(var z in obj.projects[i].image)
            { //this will create objects for the all images in the array images in the object project
              text += "<img src='" + obj.projects[i].image[z] +"' width='100' height='100' onclick='enlarge(this)' >";
            }

    text += "</div> </div> <div class = 'right-side'>"+
            "<div class='logo'> <img src='"+obj.projects[i].logo +"' width ='150'></div>" +
            "<div class ='tasks'>Progress<br> <b>Tasks: " 
            +obj.projects[i].tasksCompleted+ " out of " +obj.projects[i].tasksTotal + 
            "</b></div>"+
            "<div><a  href='"+obj.projects[i].url +"'>Github</a></div>"+
            "</div> </div></div><br>"
  }
  document.getElementById('project-card-container').innerHTML = text;
  //console.log(obj.projects[1].tasksTotal);
}

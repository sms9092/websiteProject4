<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="scripts/nav-bar.js"></script>
    <link rel="stylesheet" type="text/css" href="stylesheet/nav.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="stylesheet/style.css" media="screen" />

    <title>Projects</title>
<style>
  #project-card-container{
    padding-top: 10px;
    scroll-behavior: smooth;
    width: 80%;
    margin: auto;
  }

    .projects{
      margin-top: 50px;
      border: 1px solid black;
      background-color: whitesmoke;
      padding-left: 10px;
      padding-right: 10px;
      border-radius: 3px;
      margin-bottom: 20px;
    }
    .divided{
      display: flex;
    }
    .left-side{
      flex: 1;
    }
    .right-side{
      display: flex;
      border-left: 3px dashed silver;
      flex-flow: column;
      padding-left: 10px;
    }
    .app-images > img{
      padding: 5px 5px 5px 5px;
    }
    .right-side > div{
      text-align:center;
      align-self: center;
      padding-top: 5px;
      margin: auto;
      
    }
    body{
    background: linear-gradient(120deg, #2980b9,#8e44ad);
  }
  </style>
  </head>
  <?php
session_start();
      if(isset($_SESSION['logged']))
      {
        echo "<div id ='logged-in'></div>";
      }
      ?>
  <body onload="jsonAjax()">
    <div id="nav_bar"></div>

    <div id="project-card-container" onload="checkUrl()"></div>
    
    <div id="myModal" class="modal">
      <!-- The Close Button -->
      <span class="close">&times;</span>
    
      <!-- Modal Content (The Image) -->
      <img class="modal-content" id="img01">
    
      <!-- Modal Caption (Image Text) -->
      <div id="caption"></div>
    </div>

  </body>
  <script src="scripts/master.js"></script>
</html>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="scripts/nav-bar.js"></script>
  <link rel="stylesheet" type="text/css" href="stylesheet/nav.css" media="screen" />

  <title>HomePage</title>

  <style>
    img{
      width: 250px;
    }
    .divided {
      display: flex;
      margin: auto;
    }
    .page{
      margin-top: 100px;
      width: 70%;
      border-bottom: 3px dashed silver;
      border-top: 3px dashed silver;
    }

    .right-side{
      width:80%
    }
    .left-side {
      width: 30%;
    }
    .page-container{
      margin-top: 30px;
    }

    body {
      background: linear-gradient(120deg, #2980b9, #8e44ad);
    }
  </style>
</head>

<body>
  <?php
      session_start();
      if(isset($_SESSION['logged']))
      {
        echo "<div id ='logged-in'></div>";
      }
      ?>
  <div id="nav_bar"></div>


  <div class='page-container'>

    <div class='page divided'>
      <div class='left-side'>
        <img src='images/index.jpg' />
      </div>
      <div class='right-side'>
        <div class='desc'>
          <h2>Bio Page</h2>
          <b>A page where you can learn more about me. It also consists of a form from which you can contact me if need be.</b>
        </div>
      </div>
    </div>

    <div class='page divided'>
      <div class='left-side'>
        <img src='images/blog.jpg' />
      </div>
      <div class='right-side'>
        <div class='desc'>
          <h2>Blog Page</h2>
          <b>A page where I write blogs and update you on my life. This page is dynamic and will update according to my login status.</b>
        </div>
      </div>
    </div>

    <div class='page divided'>
      <div class='left-side'>
        <img src='images/imagevault.jpg' />
      </div>
      <div class='right-side'>
        <div class='desc'>
          <h2>Image-Vault</h2>
          <b>This page consists of pictures uploaded by me of places i've visted or images that a like</b>
        </div>
      </div>
    </div>

    <div class='page divided'>
      <div class='left-side'>
        <img src='images/unnamed.jpg' />
      </div>
      <div class='right-side'>
        <div class='desc'>
          <h2>Projects Page</h2>
          <b>This page consists of the projects that i'm working on, or that I've completed. I will keep updating my tasks tab aswell as pictures so stay tuned.</b>
        </div>
      </div>
    </div>

    <div class='page divided'>
      <div class='left-side'>
        <img src='images/music.jpg' />
      </div>
      <div class='right-side'>
        <div class='desc'>
          <h2>Music Page</h2>
          <b>This page has a table of songs that I love to listen. It's a dynamic table so keep on visiting my website for more updates on new playlist</b>
        </div>
      </div>
    </div>

  </div>


</body>

</html>
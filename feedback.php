<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="scripts/nav-bar.js"></script>
  <link rel="stylesheet" type="text/css" href="stylesheet/nav.css" media="screen" />
  <title>Feedback</title>
  <style>
      body{
        background: linear-gradient(120deg, #2980b9,#8e44ad);
      }
      .desc >p{
        border-bottom: 1px dashed brown;
        margin-left:5px;
      }
      .feedback-container{
        display: inline-flex;
        width:100%;
        margin-top: 50px;
      }
      .feedback{
        background:  #FFFF66;
        width: 15%;
        border-radius: 5px;
        border: 1px solid black;
        margin: auto;
      }
  </style>
</head>

<body>
<?php
      session_start();
      if(isset($_SESSION['logged']))
      {
        echo "<div id ='logged-in'></div>";
      }
      ?>
<div id='nav_bar'></div>

<div class='feedback-container'>
<?php
if(isset($_SESSION['logged']))
{
  
  include("fetch_feedback.php");
}else{
  echo "Please Login First";
}


?>
</div>



</body>
</html>
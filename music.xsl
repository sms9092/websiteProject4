<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html>
<head>
  <title>Music</title>
   
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="scripts/nav-bar.js"></script>
    <link rel="stylesheet" type="text/css" href="stylesheet/nav.css" media="screen" />
   
    <style type ="text/css">

      .table-container{
        display: flex;
        flex-flow: column;
        width: 100%;
        margin-top: 20px;
      }
      table{
        width: 50%;
        margin: auto;
        background: whitesmoke;
        font-size: large;
        margin-top: 25px;
        border-radius: 10px;
      }
      table > td{
        font-family: 'Courier New', Courier, monospace;
      }
      .heading{
       border-radius: 25px;
        text-align: center;
        width: 20%;
        margin: auto;
        background: grey;
      }
      
       body {
      background: linear-gradient(120deg, #2980b9, #8e44ad);
    }

    </style>
</head>
<body>
<div id="nav_bar"></div>
<div class = 'Table-container'>
  
  <div class = 'heading'><h2>My Favorite Songs</h2></div>
<div class='table'>
  <table border="1">
  <tr>
    <th>Title</th>
    <th>Album</th>
    <th>Artist</th>
  </tr>
    <xsl:for-each select="catalog/cd">
    <tr>
      <td><xsl:value-of select="title"/></td>
      <td><xsl:value-of select="album"/></td>
      <td><xsl:value-of select="artist"/></td>
    </tr>
    </xsl:for-each>
  </table>
  </div>
  </div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>


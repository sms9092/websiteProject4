<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="scripts/nav-bar.js"></script>
  <link rel="stylesheet" type="text/css" href="stylesheet/nav.css" media="screen" />

  <style>
    body {
      background: linear-gradient(120deg, #2980b9, #8e44ad);
    }
    .section1{
      text-align: center;
      width: 25%;
      margin: auto;
      margin-top: 30px;
    }
    .photo-desc{
      background-color: aliceblue;
      margin: auto;
      display: flex;
      flex-flow: column;
    }
    .photo-desc > b{
      font-size: large;
      font-family: 'Courier New', Courier, monospace;
    }
    .section2{
      width: 80%;
      margin: auto;
      background: whitesmoke;
      border-radius: 5px;
      display: flex;
      flex-flow: column;
      margin-top: 50px;
    }
    .heading{
      padding: 5px 0 5px 10px; 
      border-bottom: 1px solid silver;
      font-weight: bold;
      font-size: 45px;
      font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
    }
    .desc{
      padding: 5px 0px 5px 10px;
      font-weight: 100;
      font-family: Arial, Helvetica, sans-serif;
      font-size: xx-large;
    }
    .quote{
      padding: 5px 0px 5px 10px;
      font-weight: 50;
      font-style: italic;
      font-family: Arial, Helvetica, sans-serif;
      font-size: large;
    }
    .section3{
      width: 80%;
      margin: auto;
      margin-top: 100px;
    
    }
    .contact-me{
      width: 50%;
      margin: auto;
    }
    form{
      display: flex;
      flex-flow: column;
     
      border-radius: 5px;
    }
    form >input{
      font-size: xx-large;
      border: 1px solid dimgrey;
      border-radius: 5px;
      margin-bottom: 10px;
    }
    form >textarea{
      border: 1px solid dimgrey;
      border-radius: 5px;
      font-size: x-large;
      margin-bottom: 10px;
    }
    form .submit{
      font-size: large;
      width: 20%;
      text-align: center;

    }
    
  </style>

  <title>Bio/About Me</title>
</head>

<body>
  <?php
      session_start();
      if(isset($_SESSION['logged']))
      {
        echo "<div id ='logged-in'></div>";
      }
      ?>
  <div id="nav_bar"></div>


  <div class='profile-container'>
    
    <div class='section1' id='#'>
      <img style="border: 1px solid black;" src='images/profile1.jpg' width="250" />
      <div class='photo-desc' style="margin: auto;">
        <b>Name: Sameer Shabbir</b>
        <b style="border: 1px solid silver;">DOB: 23/04/97</b>
        <b style="border: 1px solid silver;">Email: sms9092@live.com</b>
      </div>
    </div>

    <div class='section2' id='#'>
      <b class = 'heading'>About Me</b>
      <b class="desc" style="border-bottom: 1px solid silver;">
        Computer Science student at the university of Wollonggong. I have experience working on native android and React Native.
        I'm working on increasing my skill in ML and AI.
        Since data science and AI are being developed and perfected rapidly within the last few years and their applications have increased exponentionally.  
      </b>
      <b class = "quote" style="text-align: center; width: 50%; margin: auto;">"You gain exp when you make a mistake. Earn enough and you'll level up."</b>
    </div>

    <div class='section3' id='#'>
      <div class='contact-me'>
        <h2 style="text-align: center;" >CONTACT ME<h2>
        <form action="process_contact.php" method="post" name="user">
          <input type="text" name="name" value="" placeholder="Name" required>
          <input type="text" name="email" value="" required placeholder="someone@email.com">
          <textarea name="message" required >Enter message here</textarea>

          <input class ="submit" type="submit" name="submit" value="Send" >
        </form>
      </div>
    </div>

  </div>
</body>

</html>
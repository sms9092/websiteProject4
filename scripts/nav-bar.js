$(document).ready(function(){
    $.ajax({
        type: "GET",
        url: "data/nav.xml",
        dataType: "xml",
        success: function(xml){
            var ul_main=$("<ul id='list' />");
            //console.log($(xml).find("Menu"));
            //create li element for each item in menu-root
            $(xml).find("Menu").each(function(){
                
                //adds active class to navbar items to indicate which page is active
                if(window.location.href == $(this).attr("url") ){ 
                    ul_main.append("<li class='active' id="+$(this).attr("id")+"><a href="+$(this).attr("url")+">"+$(this).attr("text")+"</a></li>");
                }else{
                    //changes the login element text to logout when user is logged in
                    if(document.getElementById('logged-in') != null && $(this).attr("id") == 'login')
                    {ul_main.append("<li id="+$(this).attr("id")+"><a href="+$(this).attr("url")+">Logout</a></li>");
                    }else{
                        ul_main.append("<li id="+$(this).attr("id")+"><a href="+$(this).attr("url")+">"+$(this).attr("text")+"</a></li>");
                    }
                }
            });
            //appends UL element to nav_bar div element
            $("#nav_bar").append(ul_main);
           
            
        }
    });
});

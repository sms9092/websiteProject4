<!DOCTYPE html>
<html>

<head>
    <title>My Blogs</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="scripts/nav-bar.js"></script>
    <script src="scripts/master.js"></script>
    <link rel="stylesheet" type="text/css" href="stylesheet/nav.css" media="screen" />

    <style>
        .blogs {
            width: 70%;
            border: 1px solid black;
            border-radius: 20px;
            margin: auto;
            background: white;
            margin-top: 10px;
        }

        #blogs-container {
            width: 70%;
            height: 100%;
            border-left: 4px solid grey;
            border-right: 4px solid grey;
            margin: auto;
            background: linear-gradient(120deg, darkgrey, grey);
            padding-top: 10px;


        }

        h1 {
            padding-left: 5px;
            border-bottom: 1px dashed grey;
        }

        #blog-body {
            padding-left: 5px;
        }

        #like {
            width: 50%;
        }

        #blog-extra {
            border-top: 1px solid black;
            display: flex;
            flex-wrap: wrap;
            padding: 10px 10px 10px 10px;
        }

        #date {
            width: 50%;
            text-align: right;

        }

        #form-container {
            margin-top: 10%;
            width: 50%;
        }
        .divided{
            display: flex;
        }

        .form{
            background: grey;
           position: sticky;
           top: 0;
           border: 1px solid black;
        }
       
        #title,#body>label{
            font-size: large;    
        }
        #title, #body{
            display: flex;
            flex-flow: column wrap;
            padding: 0px 10px 0px 10px;
        }
        #body textarea{
            border-radius: 5px;
        }
        #title input{
            border-radius: 5px;
        }

        #submit button{
            background-color: dimgray;
            size: 20px;
            color: white;
            font-size: large;

        }
        
        #submit{
            text-align: right;
            padding-right: 5%;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .like-btn{
	        border-radius: 5px;
	        background: none;
	        cursor: pointer;
	        transition: background 0.5s ease;
        }
        body{
    background: linear-gradient(120deg, #2980b9,#8e44ad);
  }

        
    </style>

</head>

<body>

    <?php
        session_start();
      if(isset($_SESSION['logged']))
      {
        echo "<div id ='logged-in'></div>";
      }
      ?>
    <div id="nav_bar"></div>
    <div class='divided'>
     <?php
        if(isset($_SESSION['logged']))
        {
            echo "   <div id='form-container'>
            <div class = 'form-heading'>
                <h1>Write your posts here and publish it on your blog.</h1>
            </div>
            <form action='process_blog.php' method='post' class='form'>
                <div id='title'>
                    <label>Title</label>
                    <input type='text' name='title' value='' required>
                </div>
                <div id='body'>
                    <label>Body</label>
                    <textarea name='body' required ></textarea>
                </div>
                <div id='submit'>
                    <button type='submit' name='submit' >Submit</button>
                </div>
            </form>
        </div>";
        }
     ?>

        <div id="blogs-container">
            <?php
            include("fetch_blog.php");
        ?>
        </div>
    </div>
        

</body>

</html>